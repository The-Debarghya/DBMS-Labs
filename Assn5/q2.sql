CREATE OR REPLACE FUNCTION add_service() RETURNS TRIGGER AS $$
DECLARE
    last_entry_no integer;
BEGIN
    SELECT COALESCE(MAX(entry_no), 0) INTO last_entry_no FROM service_log;
    INSERT INTO service_log(entry_no, req_no, service_dt)
    VALUES (last_entry_no+1, OLD.req_no, CURRENT_DATE);
    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER service_trigger
    AFTER DELETE ON request
    FOR EACH ROW 
    EXECUTE FUNCTION add_service();