CREATE OR REPLACE FUNCTION manage_backpaper() RETURNS TRIGGER AS $$
BEGIN
    --updation with more marks
    IF EXISTS (
        SELECT * FROM result WHERE roll = NEW.roll AND scode = NEW.scode AND NEW.marks >= 50
    ) THEN
        DELETE FROM backpaper WHERE roll = NEW.roll AND scode = NEW.scode;
    --updation with less marks
    ELSIF EXISTS (
        SELECT * FROM result WHERE roll = NEW.roll AND scode = NEW.scode AND NEW.marks < 50
    ) THEN
        IF NOT EXISTS (SELECT * FROM backpaper WHERE roll = NEW.roll AND scode = NEW.scode) THEN
            INSERT INTO backpaper(roll, scode) VALUES (NEW.roll, NEW.scode);
        END IF;
    --New record entry case
    ELSE
        IF (NEW.marks < 50) THEN 
            INSERT INTO backpaper(roll, scode) VALUES (NEW.roll, NEW.scode);
        END IF;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER manage_backpaper_trigger
    BEFORE INSERT OR UPDATE ON result
    FOR EACH ROW
    EXECUTE FUNCTION manage_backpaper();