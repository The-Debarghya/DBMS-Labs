# DBMS-Labs
- Contains assignments given as a part of Database Labs in BCSE course.
- `Assn0`: Basic UI for an employee management.
- `Assn1-Assn2`: SQL examples.
- `Assn3-Assn5`: PL/PgSQL and triggers.

**P.S**: All SQL statements are according to postgreSQL.
