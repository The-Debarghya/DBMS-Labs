CREATE FUNCTION find_empname(empcode varchar(5)) RETURNS varchar AS $$
BEGIN
    IF EXISTS (SELECT * FROM employee WHERE code = empcode) THEN
        RETURN (SELECT name FROM employee WHERE code = empcode);
    ELSE
        RETURN 'Not Found!';
    END IF;
END;
$$ LANGUAGE plpgsql;

\prompt 'Enter Code: ' empcode
SELECT find_empname(:empcode);