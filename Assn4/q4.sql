CREATE FUNCTION delete_employees(deptcode varchar(5)) RETURNS integer AS $$
DECLARE
    del_count integer;
BEGIN
    IF EXISTS (SELECT * FROM org o WHERE o.code = deptcode) THEN
        DELETE FROM employee e WHERE e.dept_code = deptcode;
        GET DIAGNOSTICS del_count = ROW_COUNT;
        RETURN del_count;
    ELSE
        RAISE NOTICE 'No Dept with the code exists';
        RETURN 0;
    END IF;
END;
$$ LANGUAGE plpgsql;

\prompt 'Enter Dept Code: ' deptcode
SELECT delete_employees(:deptcode);