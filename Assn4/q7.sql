CREATE OR REPLACE FUNCTION find_pending(from_date date, to_date date) RETURNS VOID AS $$
DECLARE
    orderno varchar;
    deliveryno varchar;
    orderno_cur CURSOR FOR SELECT order_no FROM ordermast om WHERE from_date <= om.order_dt AND om.order_dt <= to_date;
    row_record record;
BEGIN
    OPEN orderno_cur;
    LOOP
        FETCH orderno_cur INTO orderno;
        EXIT WHEN NOT FOUND;
        SELECT delv_no INTO deliveryno FROM deliverymast dm WHERE dm.order_no = orderno;
        FOR row_record IN SELECT order_no, item_no FROM orderdetails 
        WHERE order_no = orderno AND qty > (
            SELECT COALESCE(SUM(qty), 0) FROM deliverydetails 
            WHERE delv_no = deliveryno AND item_no = orderdetails.item_no
        ) LOOP
            RAISE NOTICE 'ORDER NO:%, ITEM NO:%', row_record.order_no, row_record.item_no;
        END LOOP;
    END LOOP;
    CLOSE orderno_cur;
END;
$$ LANGUAGE plpgsql;