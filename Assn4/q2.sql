CREATE OR REPLACE FUNCTION add_empname(
    code varchar(6),
    name varchar,
    address varchar,
    city varchar,
    basic integer,
    join_date date,
    dept_code char(5),
    grade char(1)
) RETURNS varchar AS $$
BEGIN
    IF NOT EXISTS (SELECT * FROM org o WHERE o.code = dept_code) THEN
        RETURN 'Department does not exist';
    END IF;
    IF EXISTS (SELECT * FROM employee e WHERE e.code = code) THEN
        RETURN 'Employee ID already exists!';
    END IF;
    INSERT INTO employee VALUES(code,name,address,city,basic,join_date,dept_code,grade);
    RETURN 'Insertion Successful';
END;
$$ LANGUAGE plpgsql;

\prompt 'Enter Code: ' code
\prompt 'Enter Name: ' name
\prompt 'Enter address: ' address
\prompt 'Enter city: ' city
\prompt 'Enter Basic: ' basic
\prompt 'Enter Join date: ' join_date
\prompt 'Enter Dept code: ' dept_code
\prompt 'Enter Grade: ' grade
SELECT add_empname(:code,:name,:address,:city,:basic,:join_date,:dept_code,:grade);