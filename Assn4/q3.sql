CREATE OR REPLACE FUNCTION display_highest_paid() RETURNS VOID AS $$
DECLARE
    empname varchar;
    salary numeric;
    counter integer := 0;
    cur_emp CURSOR FOR SELECT name, basic FROM employee ORDER BY basic DESC LIMIT 5;
BEGIN
    OPEN cur_emp;
    LOOP
        FETCH cur_emp INTO empname, salary;
        EXIT WHEN NOT FOUND;
        counter := counter + 1;
        RAISE NOTICE 'Employee #% is % with salary %', counter, empname, salary;
    END LOOP;
    CLOSE cur_emp;
END;
$$ LANGUAGE plpgsql;