CREATE OR REPLACE FUNCTION check_availability() RETURNS TRIGGER AS $$
BEGIN
    --Return case
    IF (NEW.issue_date IS NULL AND NEW.return_date IS NOT NULL) THEN
        IF NOT EXISTS (
            SELECT * FROM transaction_slip WHERE book_id = NEW.book_id 
            AND book_serial = NEW.book_serial
            AND member_id = NEW.member_id
            AND return_date IS NULL
        ) THEN
            RAISE EXCEPTION 'Book was not issued to Member:%', NEW.member_id;
        END IF;
        IF NOT EXISTS ( 
            SELECT * FROM book WHERE book_id = NEW.book_id 
            AND serial_num = NEW.book_serial 
            AND available = false
        ) THEN
            RAISE EXCEPTION 'Book Already Returned!';
        ELSE
            UPDATE book SET available = NOT available WHERE book_id = NEW.book_id AND serial_num = NEW.book_serial;
            UPDATE transaction_slip SET return_date = NEW.return_date 
            WHERE member_id = NEW.member_id AND book_id = NEW.book_id AND book_serial = NEW.book_serial AND return_date IS NULL;
        END IF;
    --Issue case
    ELSE
        IF NOT EXISTS ( 
            SELECT * FROM book WHERE book_id = NEW.book_id 
            AND serial_num = NEW.book_serial 
            AND available = true
        ) THEN
            RAISE NOTICE '% % % %', NEW.book_id, NEW.book_serial, NEW.issue_date, NEW.return_date;
            RAISE EXCEPTION 'Book Not Available!';
        ELSE
            IF (
                (SELECT COUNT(*) FROM transaction_slip t 
                WHERE t.issue_date IS NOT NULL AND t.return_date IS NULL AND t.member_id = NEW.member_id) 
                >= (SELECT max_books FROM member WHERE member.id = NEW.member_id)
            ) THEN 
                RAISE NOTICE '% % % %', NEW.book_id, NEW.book_serial, NEW.issue_date, NEW.return_date;
                RAISE EXCEPTION 'Cannot take more books!';
            END IF;
            UPDATE book SET available = NOT available WHERE book_id = NEW.book_id AND serial_num = NEW.book_serial;
            NEW.to_be_returned_by = (NEW.issue_date + INTERVAL '7 days')::DATE;
        END IF;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER transaction_trigger 
    BEFORE INSERT ON transaction_slip
    FOR EACH ROW
    EXECUTE FUNCTION check_availability();

CREATE OR REPLACE FUNCTION auto_increment() RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.serial_num IS NULL) THEN
        NEW.serial_num = COALESCE((SELECT MAX(serial_num) FROM book WHERE book_id = NEW.book_id), 0) + 1;
    ELSE
        RAISE NOTICE '% % ', NEW.book_id, NEW.serial_num;
        RAISE EXCEPTION 'Bruh!';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER before_insert_autoincrement
    BEFORE INSERT ON book
    FOR EACH ROW
    EXECUTE FUNCTION auto_increment();