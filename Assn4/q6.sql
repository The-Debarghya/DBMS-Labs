CREATE OR REPLACE FUNCTION actual_pay(month integer) RETURNS VOID AS $$
DECLARE
    empcode varchar;
    salary integer;
    leave_days integer := 0;
    total_days integer := 0;
    cur_emp CURSOR FOR SELECT code, basic FROM employee;
BEGIN
    OPEN cur_emp;
    LOOP
        FETCH cur_emp INTO empcode, salary;
        EXIT WHEN NOT FOUND;
        SELECT no_of_days INTO leave_days FROM leave l WHERE l.emp_no = empcode AND l.month = month;
        SELECT EXTRACT(day FROM (DATE ('2023-' || month || '-01') + INTERVAL '1 month - 1 day')) INTO total_days;
        salary := salary - (salary*leave_days/total_days);
        RAISE NOTICE 'Employee: %, will have salary:% in month:%', empcode, salary, month;
    END LOOP;
    CLOSE cur_emp;
END;
$$ LANGUAGE plpgsql;

SELECT actual_pay(2);