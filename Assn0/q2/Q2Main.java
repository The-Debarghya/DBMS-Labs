import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle;
/*
 * Created by JFormDesigner on Sat Jan 21 21:14:00 IST 2023
 */


/**
 * @author debarghya
 */
/**
 * Student
 */
class Student {
	private Integer roll;
	private String name;
	private String dcode;
	private String address;
	private String phone;
	
	public Student(Integer roll, String name, String dcode, String address, String phone) {
		this.roll = roll;
		this.name = name;
		this.dcode = dcode;
		this.address = address;
		this.phone = phone;
	}

	public Integer getRoll() {
		return this.roll;
	}

	public String getName() {
		return this.name;
	}

	public String getDcode() {
		return this.dcode;
	}

	public String getAddress() {
		return this.address;
	}

	public String getPhone() {
		return this.phone;
	}

	@Override
	public String toString() {
		return "Roll" + this.roll + ", Name:" + this.name + ", Dept. Code:" + this.dcode + ", Address:" + this.address + ", Phone No.(+91):" + this.phone;
	}
}

/**
 * StudentList
 */
class StudentList {
	private HashMap<Integer, Student> stMap = new HashMap<Integer, Student>();

	public void add(Integer roll, Student s) {
		stMap.put(roll, s);
	}

	public Student get(Integer roll) {
		return stMap.get(roll);
	}

	public void remove(Integer roll) {
		stMap.remove(roll);
	}

	public boolean exists(Integer roll) {
		return stMap.containsKey(roll);
	}

	public ArrayList<Student> retrieveStudents() {
		return new ArrayList<Student>(stMap.values());
	}

	public ArrayList<Integer> retrieveRolls() {
		return new ArrayList<Integer>(stMap.keySet());
	}
}

/**
 * Department
 */
class Department {
	private String dcode;
	private String dname;

	public Department(String dcode, String dname) {
		this.dcode = dcode;
		this.dname = dname;
	}

	public String getDcode() {
		return this.dcode;
	}

	public String getDname() {
		return this.dname;
	}	
}

/**
 * DeptList
 */
class DeptList {
	private HashMap<String, Department> dlist = new HashMap<String, Department>();

	public void add(String dcode, Department dept) {
		dlist.put(dcode, dept);
	}

	public Department get(String key) {
		return dlist.get(key);
	}

	public boolean exists(String dcode) {
		return dlist.containsKey(dcode);
	}

	public String[] retrieveCodes() {
		return dlist.keySet().toArray(new String[dlist.size()]);
	}
}

public class Q2Main extends JFrame {
	public Q2Main() {
		initComponents();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
		// Generated using JFormDesigner Evaluation license - Debarghya Maitra
		button1 = new JButton();
		button2 = new JButton();
		button3 = new JButton();
		button4 = new JButton();
		button5 = new JButton();
		button6 = new JButton();
		button7 = new JButton();
		label1 = new JLabel();
		label2 = new JLabel();

		//======== this ========
		setTitle("Main Form");
		Container contentPane = getContentPane();

		//---- button1 ----
		button1.setText("Add ");

		//---- button2 ----
		button2.setText("Edit");

		//---- button3 ----
		button3.setText("Search");

		//---- button4 ----
		button4.setText("Delete");

		//---- button5 ----
		button5.setText("Display All");

		//---- button6 ----
		button6.setText("Add");

		//---- button7 ----
		button7.setText("Display All");

		//---- label1 ----
		label1.setText("Student Record:");
		label1.setHorizontalAlignment(SwingConstants.CENTER);
		label1.setFont(new Font("sansserif", Font.ITALIC, 14));

		//---- label2 ----
		label2.setText("Dept. Record:");
		label2.setHorizontalAlignment(SwingConstants.CENTER);
		label2.setFont(new Font("sansserif", Font.ITALIC, 14));

		GroupLayout contentPaneLayout = new GroupLayout(contentPane);
		contentPane.setLayout(contentPaneLayout);
		contentPaneLayout.setHorizontalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addGap(258, 258, 258)
					.addComponent(button5, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
					.addGap(0, 276, Short.MAX_VALUE))
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addGap(93, 93, 93)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addComponent(button6, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 219, Short.MAX_VALUE)
							.addComponent(button7, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE))
						.addGroup(GroupLayout.Alignment.LEADING, contentPaneLayout.createSequentialGroup()
							.addComponent(button1, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 219, Short.MAX_VALUE)
							.addComponent(button2, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE))
						.addGroup(GroupLayout.Alignment.LEADING, contentPaneLayout.createSequentialGroup()
							.addComponent(button3, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 219, Short.MAX_VALUE)
							.addComponent(button4, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)))
					.addGap(103, 103, 103))
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addGroup(contentPaneLayout.createParallelGroup()
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addGap(196, 196, 196)
							.addComponent(label1, GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE))
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addGap(199, 199, 199)
							.addComponent(label2, GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(206, Short.MAX_VALUE))
		);
		contentPaneLayout.setVerticalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addGap(22, 22, 22)
					.addComponent(label1, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(button2)
						.addComponent(button1))
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(button5)
					.addGap(5, 5, 5)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(button3)
						.addComponent(button4))
					.addGap(39, 39, 39)
					.addComponent(label2, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
					.addGap(31, 31, 31)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(button7)
						.addComponent(button6))
					.addContainerGap(78, Short.MAX_VALUE))
		);
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
		ActionListener addStListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (dlist.retrieveCodes().length == 0) {
						throw new Exception("No Departments Added Yet!");
					}
					AddStForm addform = new AddStForm(stlist, dlist);
					addform.setSize(570, 400);
					addform.setVisible(true);
					addform.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(new JPanel(), e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
		};
		button1.addActionListener(addStListener);

		ActionListener editListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				EditForm editfrom = new EditForm(stlist, dlist);
				editfrom.setSize(400, 215);
				editfrom.setVisible(true);
				editfrom.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			}
		};
		button2.addActionListener(editListener);

		ActionListener searchListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				SearchForm searchform = new SearchForm(stlist);
				searchform.setSize(400, 215);
				searchform.setVisible(true);
				searchform.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			}
		};
		button3.addActionListener(searchListener);

		ActionListener deleteListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				DeleteForm delform = new DeleteForm(stlist);
				delform.setSize(400, 215);
				delform.setVisible(true);
				delform.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			}
		};
		button4.addActionListener(deleteListener);

		ActionListener displayStListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				DisplayStForm dispform = new DisplayStForm(stlist, dlist);
				dispform.setSize(785, 295);
				dispform.setVisible(true);
				dispform.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			}
		};
		button5.addActionListener(displayStListener);

		ActionListener addDeptListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AddDeptForm addform = new AddDeptForm(dlist);
				addform.setSize(435, 260);
				addform.setVisible(true);
				addform.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			}
		};
		button6.addActionListener(addDeptListener);

		ActionListener displayDeptListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				DisplayDeptForm dispform = new DisplayDeptForm(dlist);
				dispform.setSize(455, 355);
				dispform.setVisible(true);
				dispform.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			}
		};
		button7.addActionListener(displayDeptListener);
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
	// Generated using JFormDesigner Evaluation license - Debarghya Maitra
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6;
	private JButton button7;
	private JLabel label1;
	private JLabel label2;
	private DeptList dlist = new DeptList();
	private StudentList stlist = new StudentList();
	// JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
	public static void main(String[] args) {
		Q2Main qform = new Q2Main();
		qform.setSize(655, 380);
		qform.setVisible(true);
		qform.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
