import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle;
import javax.swing.border.*;
/*
 * Created by JFormDesigner on Sat Jan 21 21:38:33 IST 2023
 */



/**
 * @author debarghya
 */
public class EditForm extends JFrame {
	public EditForm(StudentList stlist, DeptList dlist) {
		initComponents(stlist, dlist);
	}

	private void initComponents(StudentList stlist, DeptList dlist) {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
		// Generated using JFormDesigner Evaluation license - Debarghya Maitra
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		label3 = new JLabel();
		textField1 = new JTextField();
		buttonBar = new JPanel();
		okButton = new JButton();
		cancelButton = new JButton();

		//======== this ========
		setTitle("Edit Student Details");
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
			dialogPane.setBorder(new CompoundBorder(new TitledBorder(new EmptyBorder(0, 0, 0, 0), "", TitledBorder.CENTER, TitledBorder.BOTTOM, new Font("Dialog",Font.BOLD,12), Color.red),dialogPane.getBorder())); 
			dialogPane.addPropertyChangeListener(new java.beans.PropertyChangeListener() { 
				@Override 
				public void propertyChange(java.beans.PropertyChangeEvent e) {
					if ("border".equals(e.getPropertyName())) 
						throw new RuntimeException(); 
				}
			});
			dialogPane.setLayout(new BorderLayout());

			//======== contentPanel ========
			{

				//---- label3 ----
				label3.setText("Roll to Edit:");
				label3.setHorizontalAlignment(SwingConstants.TRAILING);

				GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
				contentPanel.setLayout(contentPanelLayout);
				contentPanelLayout.setHorizontalGroup(
					contentPanelLayout.createParallelGroup()
						.addGroup(contentPanelLayout.createSequentialGroup()
							.addGap(24, 24, 24)
							.addComponent(label3, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
							.addComponent(textField1, GroupLayout.PREFERRED_SIZE, 184, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(42, Short.MAX_VALUE))
				);
				contentPanelLayout.setVerticalGroup(
					contentPanelLayout.createParallelGroup()
						.addGroup(contentPanelLayout.createSequentialGroup()
							.addGap(20, 20, 20)
							.addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(textField1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(label3, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
							.addContainerGap(79, Short.MAX_VALUE))
				);
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);

			//======== buttonBar ========
			{
				buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
				buttonBar.setLayout(new GridBagLayout());
				((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
				((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

				//---- okButton ----
				okButton.setText("OK");
				buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//---- cancelButton ----
				cancelButton.setText("Cancel");
				buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});

				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						try {
							Integer roll = Integer.parseInt(textField1.getText());
							if (! stlist.exists(roll)) {
								throw new Exception("Roll Doesn't Exist");
							}
							Student st = stlist.get(roll);
							EditingForm eform = new EditingForm(st, stlist, dlist);
							eform.setSize(570, 400);
							eform.setVisible(true);
							eform.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
							dispose();
						} catch (Exception e) {
							JOptionPane.showMessageDialog(new JPanel(), e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
							e.printStackTrace();
						}
					}
				});

			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
	// Generated using JFormDesigner Evaluation license - Debarghya Maitra
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JLabel label3;
	private JTextField textField1;
	private JPanel buttonBar;
	private JButton okButton;
	private JButton cancelButton;
	// JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
