import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
/*
 * Created by JFormDesigner on Sat Jan 21 22:21:41 IST 2023
 */



/**
 * @author debarghya
 */

/**
 * BatchIterator<T> implements Iterator<T>
 */
class BatchIterator<T> implements Iterator<java.util.List<T>> {
	private int batchSize;
    private java.util.List<T> currentBatch;
    private Iterator<T> iterator;

	public BatchIterator(Iterator<T> sourceIterator, int batchSize) {
        this.batchSize = batchSize;
        this.iterator = sourceIterator;
    }

	@Override
	public java.util.List<T> next() {
		prepareNextBatch();
		return this.currentBatch;
	}

	@Override
	public boolean hasNext() {
		return this.iterator.hasNext();
	}

	private void prepareNextBatch() {
		currentBatch = new ArrayList<T>(batchSize);
		while (iterator.hasNext() && currentBatch.size() < batchSize) {
			currentBatch.add(iterator.next());
		}
	}
	
	public static <T> Stream<java.util.List<T>> batchStreamOf(Stream<T> stream, int batchSize) {
		return stream(new BatchIterator<T>(stream.iterator(), batchSize));
	}

	public static <T> Stream<T> stream(Iterator<T> iterator) {
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.NONNULL), false);
	}
}


public class DisplayStForm extends JFrame {
	public DisplayStForm(StudentList stlist, DeptList dlist) {
		initComponents(stlist, dlist);
	}

	private void initComponents(StudentList stlist, DeptList dlist) {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
		// Generated using JFormDesigner Evaluation license - Debarghya Maitra
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		table1 = new JTable();
		button1 = new JButton();
		button2 = new JButton();

		//======== this ========
		setTitle("Display Students");
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
			dialogPane.setBorder(new CompoundBorder(new TitledBorder(new EmptyBorder(0, 0, 0, 0), "", TitledBorder.CENTER, TitledBorder.BOTTOM, new Font("Dialog",Font.BOLD,12), Color.red),dialogPane.getBorder())); 
			dialogPane.addPropertyChangeListener(new java.beans.PropertyChangeListener() { 
				@Override 
				public void propertyChange(java.beans.PropertyChangeEvent e) {
					if ("border".equals(e.getPropertyName())) 
						throw new RuntimeException(); 
				}
			});
			dialogPane.setLayout(new BorderLayout());

			//======== contentPanel ========
			{
				DefaultTableModel model = new DefaultTableModel(
					new Object[][] {},
					new String[] {
						"Roll", "Name", "Phone", "Address", "Dept. Code", "Dept. Name"
					});
				//---- table1 ----
				table1.setShowVerticalLines(true);
				table1.setShowHorizontalLines(true);
				table1.setGridColor(new Color(0x333333));
				table1.setSelectionBackground(new Color(0x33ffff));
				table1.setBorder(new TitledBorder(null, "", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION));
				
				ArrayList<Integer> rolls = stlist.retrieveRolls();
				final AtomicInteger page = new AtomicInteger(0);
				final AtomicInteger counter = new AtomicInteger();
				ArrayList<ArrayList<Integer>> groupedRolls = new ArrayList<>();
				for (Integer roll : rolls) {
					if (counter.getAndIncrement() % 5 == 0) {
						groupedRolls.add(new ArrayList<>());
					}
					groupedRolls.get(groupedRolls.size()-1).add(roll);
				}
				if (groupedRolls.size() == 1) {
					button2.setEnabled(false);
				}
				button1.setEnabled(false);
				try {
					for (Integer roll : groupedRolls.get(0)) {
						Student s = stlist.get(roll);
						Object[] row = {roll, s.getName(), s.getPhone(), s.getAddress(), s.getDcode(), dlist.get(s.getDcode()).getDname()};
						model.addRow(row);
					}
					table1.setModel(model);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(new JPanel(), e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}

				button1.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						try {
							int p = page.decrementAndGet();
							for (Integer roll : groupedRolls.get(p)) {
								Student s = stlist.get(roll);
								Object[] row = {roll, s.getName(), s.getPhone(), s.getAddress(), s.getDcode(), dlist.get(s.getDcode()).getDname()};
								for (int i = 0; i < 5; i++) {
									for (int j = 0; j < 6; j++) {
										model.setValueAt(row[j], i, j);
									}
								}
							}
							table1.setModel(model);
							if (! button2.isEnabled()) {
								button2.setEnabled(true);
							}
						} catch (IndexOutOfBoundsException e) {
							button1.setEnabled(false);
						} catch (Exception e) {
							JOptionPane.showMessageDialog(new JPanel(), e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
							e.printStackTrace();
						}
					}
				});
				button2.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						try {
							int p = page.incrementAndGet();
							for (Integer roll : groupedRolls.get(p)) {
								Student s = stlist.get(roll);
								Object[] row = {roll, s.getName(), s.getPhone(), s.getAddress(), s.getDcode(), dlist.get(s.getDcode()).getDname()};
								for (int i = 0; i < 5; i++) {
									for (int j = 0; j < 6; j++) {
										model.setValueAt(row[j], i, j);
									}
								}
							}
							table1.setModel(model);
							if (! button1.isEnabled()) {
								button1.setEnabled(true);
							}
						} catch (IndexOutOfBoundsException e) {
							button2.setEnabled(false);
						} catch (Exception e) {
							JOptionPane.showMessageDialog(new JPanel(), e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
							e.printStackTrace();
						}
					}
				});

				{
					TableColumnModel cm = table1.getColumnModel();
					cm.getColumn(0).setMinWidth(100);
					cm.getColumn(0).setMaxWidth(150);
					cm.getColumn(0).setPreferredWidth(100);
					cm.getColumn(1).setMinWidth(100);
					cm.getColumn(1).setMaxWidth(150);
					cm.getColumn(1).setPreferredWidth(100);
					cm.getColumn(2).setMinWidth(100);
					cm.getColumn(2).setMaxWidth(150);
					cm.getColumn(2).setPreferredWidth(100);
					cm.getColumn(3).setMinWidth(200);
					cm.getColumn(3).setMaxWidth(250);
					cm.getColumn(3).setPreferredWidth(200);
					cm.getColumn(4).setMinWidth(90);
					cm.getColumn(4).setMaxWidth(100);
					cm.getColumn(4).setPreferredWidth(90);
					cm.getColumn(5).setMinWidth(90);
					cm.getColumn(5).setMaxWidth(100);
					cm.getColumn(5).setPreferredWidth(90);
				}

				//---- button1 ----
				button1.setText("PREV");

				//---- button2 ----
				button2.setText("NEXT");

				GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
				contentPanel.setLayout(contentPanelLayout);
				contentPanelLayout.setHorizontalGroup(
					contentPanelLayout.createParallelGroup()
						.addGroup(contentPanelLayout.createSequentialGroup()
							.addGroup(contentPanelLayout.createParallelGroup()
								.addGroup(contentPanelLayout.createSequentialGroup()
									.addGap(31, 31, 31)
									.addComponent(table1, GroupLayout.DEFAULT_SIZE, 702, Short.MAX_VALUE))
								.addGroup(contentPanelLayout.createSequentialGroup()
									.addGap(234, 234, 234)
									.addComponent(button1, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
									.addGap(37, 37, 37)
									.addComponent(button2, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
							.addContainerGap(26, Short.MAX_VALUE))
				);
				contentPanelLayout.setVerticalGroup(
					contentPanelLayout.createParallelGroup()
						.addGroup(contentPanelLayout.createSequentialGroup()
							.addComponent(table1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGap(18, 18, 18)
							.addGroup(contentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(button1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(button2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
							.addContainerGap())
				);
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
	// Generated using JFormDesigner Evaluation license - Debarghya Maitra
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JTable table1;
	private JButton button1;
	private JButton button2;
	// JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
