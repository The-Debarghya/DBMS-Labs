import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle;
import javax.swing.table.*;
/*
 * Created by JFormDesigner on Sat Jan 21 22:27:27 IST 2023
 */



/**
 * @author debarghya
 */
public class DisplayDeptForm extends JFrame {
	public DisplayDeptForm(DeptList dlist) {
		initComponents(dlist);
	}

	private void initComponents(DeptList dlist) {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
		// Generated using JFormDesigner Evaluation license - Debarghya Maitra
		scrollPane1 = new JScrollPane();
		table1 = new JTable();
		button1 = new JButton();

		//======== this ========
		setTitle("Department List");
		Container contentPane = getContentPane();

		//======== scrollPane1 ========
		{

			//---- table1 ----
			DefaultTableModel model = new DefaultTableModel(
				new Object[][] {},
				new String[] {
					"Dept. Code", "Dept. Name"
				}
			);
			for (String d : dlist.retrieveCodes()) {
				Object[] row = {d, dlist.get(d).getDname()};
				model.addRow(row);
			}
			table1.setModel(model);
			{
				TableColumnModel cm = table1.getColumnModel();
				cm.getColumn(0).setMinWidth(90);
				cm.getColumn(0).setMaxWidth(100);
				cm.getColumn(0).setPreferredWidth(90);
			}
			table1.setGridColor(new Color(0x333333));
			table1.setSelectionBackground(new Color(0x33ffff));
			scrollPane1.setViewportView(table1);
		}

		//---- button1 ----
		button1.setText("OK");
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});

		GroupLayout contentPaneLayout = new GroupLayout(contentPane);
		contentPane.setLayout(contentPaneLayout);
		contentPaneLayout.setHorizontalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addGroup(contentPaneLayout.createParallelGroup()
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addGap(63, 63, 63)
							.addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE))
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addGap(173, 173, 173)
							.addComponent(button1, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(65, Short.MAX_VALUE))
		);
		contentPaneLayout.setVerticalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addGap(18, 18, 18)
					.addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
					.addComponent(button1)
					.addGap(19, 19, 19))
		);
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
	// Generated using JFormDesigner Evaluation license - Debarghya Maitra
	private JScrollPane scrollPane1;
	private JTable table1;
	private JButton button1;
	// JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
