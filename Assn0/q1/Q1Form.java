import java.awt.*;
import java.awt.event.*;
import java.sql.Timestamp;
import java.util.Date;

import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle;
/*
 * Created by JFormDesigner on Wed Jan 18 22:42:44 IST 2023
 */

/**
 * @author debarghya
 */
public class Q1Form extends JFrame {
	public Q1Form() {
		initComponents();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
		// Generated using JFormDesigner Evaluation license - Debarghya Maitra
		textField1 = new JTextField();
		textField2 = new JTextField();
		label1 = new JLabel();
		label2 = new JLabel();
		label3 = new JLabel();
		textField3 = new JTextField();
		button1 = new JButton();
		button2 = new JButton();
		label4 = new JLabel();
		label5 = new JLabel();

		//======== this ========
		setTitle("ADD or SUBTRACT");
		Container contentPane = getContentPane();

		//---- label1 ----
		label1.setText("1st No:");
		label1.setHorizontalAlignment(SwingConstants.CENTER);
		label1.setForeground(Color.darkGray);

		//---- label2 ----
		label2.setText("2nd No:");
		label2.setHorizontalAlignment(SwingConstants.CENTER);
		label2.setForeground(Color.darkGray);

		//---- label3 ----
		label3.setText("Result:");
		label3.setHorizontalAlignment(SwingConstants.CENTER);
		label3.setForeground(Color.darkGray);

		//---- button1 ----
		button1.setText("Add");

		//---- button2 ----
		button2.setText("Subtract");

		//---- label4 ----
		label4.setHorizontalAlignment(SwingConstants.CENTER);

		//---- label5 ----
		Date date = new Date();
		label5.setText("Last Update: " + new Timestamp(date.getTime()).toString());
		label5.setFont(new Font("sansserif", Font.PLAIN, 8));

		GroupLayout contentPaneLayout = new GroupLayout(contentPane);
		contentPane.setLayout(contentPaneLayout);
		contentPaneLayout.setHorizontalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addGroup(contentPaneLayout.createParallelGroup()
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addGap(146, 146, 146)
							.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
								.addGroup(contentPaneLayout.createSequentialGroup()
									.addComponent(button1, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(button2, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
								.addGroup(contentPaneLayout.createSequentialGroup()
									.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
										.addComponent(label2, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
										.addComponent(label1, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
										.addComponent(label3, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
									.addGroup(contentPaneLayout.createParallelGroup()
										.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
											.addComponent(textField2, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
											.addComponent(textField1, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE))
										.addComponent(textField3, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)))))
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addComponent(label5, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
							.addGap(90, 90, 90)
							.addComponent(label4, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(164, Short.MAX_VALUE))
		);
		contentPaneLayout.setVerticalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addComponent(label5, GroupLayout.PREFERRED_SIZE, 13, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
					.addComponent(label4)
					.addGap(26, 26, 26)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(textField1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(18, 18, 18)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(textField2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label2, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
					.addGap(27, 27, 27)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(label3, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(44, 44, 44)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(button1)
						.addComponent(button2)))
		);
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
		ActionListener aActionListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					Double a = Double.parseDouble(textField1.getText());
					Double b = Double.parseDouble(textField2.getText());
					textField3.setText(Double.toString(a+b));
				}catch(Exception err) {
					JOptionPane.showMessageDialog(new JPanel(), "Give proper values in both text fields!", "ERROR", JOptionPane.ERROR_MESSAGE);
					err.printStackTrace();
				}
			}
		};
		ActionListener subActionListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					Double a = Double.parseDouble(textField1.getText());
					Double b = Double.parseDouble(textField2.getText());
					textField3.setText(Double.toString(a-b));
				}catch(Exception err) {
					JOptionPane.showMessageDialog(new JPanel(), "Give proper values in both text fields!", "ERROR", JOptionPane.ERROR_MESSAGE);
					err.printStackTrace();
				}
			}
		};
		button1.addActionListener(aActionListener);
		button2.addActionListener(subActionListener);
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
	// Generated using JFormDesigner Evaluation license - Debarghya Maitra
	private JTextField textField1;
	private JTextField textField2;
	private JLabel label1;
	private JLabel label2;
	private JLabel label3;
	private JTextField textField3;
	private JButton button1;
	private JButton button2;
	private JLabel label4;
	private JLabel label5;
	// JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
	public static void main(String[] args) {
		Q1Form qform = new Q1Form();
		qform.setSize(550, 330);
		qform.setVisible(true);
		qform.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
}
