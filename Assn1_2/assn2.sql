INSERT INTO EMP VALUES('EMP10','heisenberg','FINAN','EXECU','M','abcd','kolkata','WEST BENGAL','700110',8787, '1969-07-31');
INSERT INTO EMP VALUES('EMP11','marie','PERCH','CLERK','F','abcd1','kolkataab','WEST BENGAL','702110',9000, '1989-01-08');
INSERT INTO EMP VALUES('EMP12','lydia','PERCH','CLERK','F','abcd2','kolkatabc','WEST BENGAL','703110',2370, '1989-12-08');
INSERT INTO EMP VALUES('EMP13','skyler','PRODU','CLERK','F','abcd3','kolkatacd','WEST BENGAL','730110',440, '1999-09-08');
INSERT INTO EMP VALUES('EMP14','xyz44','PRODU','CLERK','F','abcd4','kolkataef','WEST BENGAL','705110',780, '2001-09-08');
INSERT INTO EMP VALUES('EMP15','hank','PRODU','EXECU','M','abcd5','kolkatagh','WEST BENGAL','706110',9029, '2010-07-31');
INSERT INTO EMP VALUES('EMP16','asther','PERSO','CLERK','F','abcd6','kolkataij','WEST BENGAL','200110',880, '1989-10-08');
INSERT INTO EMP VALUES('EMP17','gus','PERSO','CLERK','M','abcd7','kolkatakl','WEST BENGAL','700310',2370, '1997-09-08');
INSERT INTO EMP VALUES('EMP18','jessie','PERSO','CLERK','M','abcd8','kolkatamn','WEST BENGAL','900110',440, '1989-06-08');
INSERT INTO EMP VALUES('EMP19','walter','PERCH','CLERK','M','abcd9','kolkataop','WEST BENGAL','500110',780, '1989-02-08');


SELECT dept_code, MAX(basic) FROM emp GROUP BY dept_code;
SELECT dept_code, MIN(basic) FROM emp GROUP BY dept_code;
SELECT dept_code, AVG(basic) FROM emp GROUP BY dept_code;

SELECT dept_code, sex, COUNT(*) FROM emp_test GROUP BY dept_code, sex HAVING sex = 'F';

SELECT e.dept_code, e.city, COUNT(*)
FROM emp e
GROUP BY e.dept_code, e.city
ORDER BY e.dept_code;

SELECT e.dept_code, e.desig_code, COUNT(*)
FROM emp e
GROUP BY e.dept_code, e.desig_code, e.jn_dt HAVING EXTRACT(YEAR FROM jn_dt) = 2000
ORDER BY COUNT(*);

SELECT * FROM (SELECT e.dept_code, SUM(basic)
FROM emp e 
GROUP BY e.dept_code, e.sex HAVING e.sex = 'M'
ORDER BY SUM(basic) DESC) AS ordering WHERE sum > 50000;

SELECT e.emp_name, d.desig_desc, e.basic
FROM emp e
JOIN designation d ON d.desig_code = e.desig_code;

SELECT e.emp_name, d.desig_desc, e.basic, dep.dept_name
FROM emp e
JOIN department dep ON dep.dept_code = e.dept_code
JOIN designation d ON d.desig_code = e.desig_code;


SELECT dept_code 
FROM department 
WHERE dept_code NOT IN (SELECT dept_code FROM emp);

SELECT DISTINCT d.dept_name
FROM department d
JOIN emp e ON d.dept_code = e.dept_code;

SELECT d.dept_name
FROM emp e
JOIN department d ON d.dept_code = e.dept_code
GROUP BY d.dept_name HAVING COUNT(*) >= 10;

SELECT dept_code 
FROM emp
WHERE basic = (SELECT MAX(basic) FROM emp);

SELECT d.desig_desc 
FROM emp e
JOIN designation d ON d.desig_code = e.desig_code
WHERE basic = (SELECT MAX(basic) FROM emp);

SELECT dept_code, COUNT(*) FROM emp_test WHERE desig_code = 'MANAG' GROUP BY dept_code;

SELECT basic FROM emp ORDER BY basic DESC LIMIT 1;
SELECT basic FROM emp ORDER BY basic ASC LIMIT 1;

SELECT d.dept_name 
FROM (SELECT dept_code, SUM(basic)
    FROM emp e
    GROUP BY dept_code
    ORDER BY SUM(basic) DESC 
    LIMIT 1) AS max_basic
JOIN department d ON d.dept_code = max_basic.dept_code;

SELECT d.dept_name 
FROM (SELECT dept_code, AVG(basic)
    FROM emp e
    GROUP BY dept_code
    ORDER BY AVG(basic) DESC 
    LIMIT 1) AS max_basic
JOIN department d ON d.dept_code = max_basic.dept_code;

SELECT d.dept_name 
FROM (SELECT dept_code, COUNT(*)
    FROM emp e
    GROUP BY dept_code
    ORDER BY COUNT(*) DESC 
    LIMIT 1) AS max_count
JOIN department d ON d.dept_code = max_count.dept_code;

DELETE FROM emp WHERE desig_code NOT IN (SELECT desig_code FROM designation );

SELECT emp_name
FROM emp e1
WHERE sex = 'F' AND basic > (
    SELECT AVG(basic)
    FROM emp e2
    WHERE e2.dept_code = e1.dept_code);

SELECT COUNT(*) FROM emp WHERE desig_code = 'MANAG' AND sex = 'F';
