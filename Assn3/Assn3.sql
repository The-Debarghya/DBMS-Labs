--Question 1
CREATE TABLE org (code VARCHAR(5), name VARCHAR(15));
ALTER TABLE org ADD PRIMARY KEY(code); 

CREATE SEQUENCE emp_seq;
SELECT setval('emp_seq', 100);

CREATE TABLE employee(
    code TEXT PRIMARY KEY CHECK (code ~ '^EMP[0-9]+$') DEFAULT 'EMP' || nextval('emp_seq'),
    name VARCHAR(20),
    address VARCHAR(20),
    city VARCHAR(15),
    basic INT,
    join_date DATE DEFAULT CURRENT_DATE,
    dept_code VARCHAR(5),
    grade CHAR(1),
    FOREIGN KEY (dept_code) REFERENCES org(code) ON DELETE RESTRICT,
    CONSTRAINT basic_range CHECK (basic >= 50000 AND basic <= 90000),
    CONSTRAINT grade_res CHECK (grade IN ('A', 'B', 'C')),
    CONSTRAINT name_cons CHECK (name = UPPER(name))
);

CREATE TABLE leave_reg(
    emp_code TEXT,
    from_date DATE,
    to_date DATE,
    type CHAR(2),
    PRIMARY KEY (emp_code, from_date)
    FOREIGN KEY (emp_code) REFERENCES employee(code) ON DELETE CASCADE,
    CONSTRAINT type_cons CHECK (type IN ('CL', 'EL', 'ML'))
);



--Question 2
INSERT INTO employee(name, address, city, basic, join_date, dept_code, grade)
VALUES ('TEST', 'Abc', 'Kolkata', 4000, '2022-11-11', 'PURCH', 'A');
INSERT INTO employee(name, address, city, basic, join_date, dept_code, grade)
VALUES ('TEST', 'Abc', 'Kolkata', 60000, '2022-11-11', 'D1', 'A');
UPDATE employee SET dept_code = 'D1' WHERE code = 'EMP289';
DELETE FROM org WHERE code = 'FINAN';



--Question 3
CREATE TABLE filtered_emp AS 
    SELECT employee.code AS emp_code, employee.name AS emp_name, org.name AS dept_name, employee.basic AS basic 
    FROM employee
    JOIN org ON org.code = employee.dept_code
    WHERE employee.dept_code = 'PURCH' AND employee.basic = 70000;

ALTER TABLE filtered_emp ADD PRIMARY KEY (emp_code);

INSERT INTO filtered_emp(emp_code, emp_name, dept_name, basic)
    SELECT employee.code, employee.name, org.name, employee.basic
    FROM employee 
    JOIN org ON dept_code = org.code AND basic >= 70000;

ALTER TABLE filtered_emp ADD COLUMN net_pay REAL;

UPDATE filtered_emp SET net_pay = basic*1.5;

ALTER TABLE filtered_emp DROP COLUMN net_pay;



--Question 5
CREATE TABLE book(
    book_id INT,
    serial_num INT,
    title VARCHAR(100),
    author VARCHAR(50),
    publisher VARCHAR(60),
    price INT,
    available BOOLEAN DEFAULT true,
    PRIMARY KEY (book_id, serial_num)
);


CREATE TABLE member(
    id INT PRIMARY KEY,
    name VARCHAR(25),
    email VARCHAR(100),
    member_type CHAR(7),
    max_books INT,
    CONSTRAINT check_type CHECK (member_type IN ('faculty', 'student'))
    CONSTRAINT check_maxbooks CHECK ((member_type = 'faculty' AND max_books = 10) OR (member_type = 'student' AND max_books = 5))
);

CREATE TABLE transaction_slip(
    id INT PRIMARY KEY,
    member_id INT,
    book_id INT,
    book_serial INT,
    issue_date DATE,
    return_date DATE,
    to_be_returned_by DATE,
    FOREIGN KEY (member_id) REFERENCES member(id) ON DELETE RESTRICT,
    FOREIGN KEY (book_id, book_serial) REFERENCES book(book_id, serial_num) ON DELETE RESTRICT
);

CREATE OR REPLACE FUNCTION check_availability() RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.issue_date IS NULL AND NEW.return_date IS NOT NULL) THEN
        IF NOT EXISTS ( SELECT * FROM book 
                    WHERE book_id = NEW.book_id AND serial_num = NEW.book_serial AND available = false
        ) THEN
            RAISE EXCEPTION 'Book Already Returned!';
        ELSE
            UPDATE book SET available = NOT available WHERE book_id = NEW.book_id AND serial_num = NEW.book_serial;
            UPDATE transaction_slip SET return_date = NEW.return_date 
            WHERE member_id = NEW.member_id AND book_id = NEW.book_id AND book_serial = NEW.book_serial AND return_date IS NULL;
        END IF;
    ELSE
        IF NOT EXISTS ( SELECT * FROM book 
                    WHERE book_id = NEW.book_id AND serial_num = NEW.book_serial AND available = true
        ) THEN
            RAISE NOTICE '% % % %', NEW.book_id, NEW.book_serial, NEW.issue_date, NEW.return_date;
            RAISE EXCEPTION 'Book Not Available!';
        ELSE
            IF (
                (SELECT COUNT(*) FROM transaction_slip t 
                WHERE t.issue_date IS NOT NULL AND t.return_date IS NULL AND t.member_id = NEW.member_id) 
                >= (SELECT max_books FROM member WHERE member.id = NEW.member_id)
            ) THEN 
                RAISE NOTICE '% % % %', NEW.book_id, NEW.book_serial, NEW.issue_date, NEW.return_date;
                RAISE EXCEPTION 'Book Not Available!';
            END IF;
            UPDATE book SET available = NOT available WHERE book_id = NEW.book_id AND serial_num = NEW.book_serial;
            NEW.to_be_returned_by = (NEW.issue_date + INTERVAL '7 days')::DATE;
        END IF;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER transaction_trigger 
    BEFORE INSERT ON transaction_slip
    FOR EACH ROW
    EXECUTE FUNCTION check_availability();

CREATE OR REPLACE FUNCTION auto_increment() RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.serial_num IS NULL) THEN
        NEW.serial_num = COALESCE((SELECT MAX(serial_num) FROM book WHERE book_id = NEW.book_id), 0) + 1;
    ELSE
        RAISE NOTICE '% % ', NEW.book_id, NEW.serial_num;
        RAISE EXCEPTION 'Bruh!';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER before_insert_autoincrement
    BEFORE INSERT ON book
    FOR EACH ROW
    EXECUTE FUNCTION auto_increment();

--Number of copies for each book(all)
SELECT book_id, COUNT(*) FROM book
GROUP BY book_id; 
--Number of copies for each book(issued)
SELECT book_id, COUNT(*) AS issued_copies FROM book
GROUP BY book_id, available HAVING available = false;

--student/faculty not making any transactions
SELECT member.* FROM member
LEFT JOIN transaction_slip AS t ON t.member_id = member.id
WHERE t.member_id IS NULL and member.member_type = 'student';
SELECT member.* FROM member
LEFT JOIN transaction_slip AS t ON t.member_id = member.id
WHERE t.member_id IS NULL and member.member_type = 'faculty';

--members holding a book after due date
SELECT t1.member_id, member.name, member.email, t1.issue_date, t1.to_be_returned_by, t1.return_date FROM
transaction_slip AS t1
JOIN member ON member.id = t1.member_id
WHERE t1.to_be_returned_by < CURRENT_DATE AND t1.return_date IS NULL;

--transaction details of late book return
SELECT * FROM transaction_slip
WHERE to_be_returned_by < return_date ;
